<?php namespace mef\RateLimit\Exception;

use mef\RateLimit\Exception\RateLimitException;

/**
 * An exception thrown when the rate limit has been exceeded.
 */
class RateLimitExceededException extends RateLimitException
{
	/**
	 * Constructor
	 *
	 * @param int    $limit        The maximum number of requests per interval.
	 * @param string $intervalSpec The interval specification.
	 */
	public function __construct(int $limit, string $intervalSpec)
	{
		parent::__construct('Rate limit of ' . $limit . ' over ' . $intervalSpec . ' exceeded.');
	}
}