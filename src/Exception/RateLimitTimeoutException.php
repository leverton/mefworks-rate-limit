<?php namespace mef\RateLimit\Exception;

use mef\RateLimit\Exception\RateLimitException;

/**
 * An exception thrown if the rate limiter did not respond in time.
 *
 * This typically indicates that there is an extremely high load, and
 * should be treated as an error.
 */
class RateLimitTimeoutException extends RateLimitException
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct('A timeout occurred when checking the rate limit.');
	}
}