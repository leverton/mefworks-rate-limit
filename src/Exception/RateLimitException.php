<?php namespace mef\RateLimit\Exception;

use Exception;

/**
 * Base class for rate limit exceptions
 */
class RateLimitException extends Exception
{
}