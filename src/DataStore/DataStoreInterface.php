<?php namespace mef\RateLimit\DataStore;

use mef\RateLimit\RateLimitItem\RateLimitItemInterface;

/**
 * A data store for the rate limiter.
 */
interface DataStoreInterface
{
	/**
	 * Retrieve a previously stored value.
	 *
	 * @param  string $key  The item's key.
	 *
	 * @return \mef\RateLimit\RateLimitItem\RateLimitItemInterface
	 */
	public function getItem(string $key) : RateLimitItemInterface;

	/**
	 * Update a previously retrieved item.
	 *
	 * @param  \mef\RateLimit\RateLimitItem\RateLimitItemInterface   $item       The item that is to be updatd.
	 * @param  array                                                 $values     The limit values
	 *
	 * @return bool  Returns true if the item was saved.
	 */
	public function updateItem(RateLimitItemInterface $item, array $values) : bool;

	/**
	 * Insert a new item
	 *
	 * @param  string   $key        The item's key.
	 * @param  array    $values     The limit values
	 *
	 * @return bool  Returns true if the item was added.
	 */
	public function insert(string $key, array $limits) : bool;
}