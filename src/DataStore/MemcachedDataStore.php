<?php namespace mef\RateLimit\DataStore;

use DateTimeImmutable;
use mef\RateLimit\DataStore\DataStoreInterface;
use mef\RateLimit\RateLimitItem\RateLimitItemInterface;
use mef\RateLimit\RateLimitItem\MemcachedRateLimitItem;
use mef\RateLimit\RateLimitItem\NullRateLimitItem;
use Memcached;

/**
 * A Memcached data store.
 */
class MemcachedDataStore implements DataStoreInterface
{
	private $memcached;

	/**
	 * Constructs the class
	 *
	 * @param  Memcached   $memcached    Memcached handler
	 */
	public function __construct(Memcached $memcached)
	{
		$this->memcached = $memcached;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getItem(string $key) : RateLimitItemInterface
	{
		$memcachedObject = $this->memcached->get($key, null, Memcached::GET_EXTENDED);

		if ($memcachedObject === false)
		{
			return new NullRateLimitItem;
		}

		$value = $memcachedObject['value'];
		$token = $memcachedObject['cas'];

		list($storageTime, $values) = explode(";", $value);

		return new MemcachedRateLimitItem($key, new DateTimeImmutable($storageTime), explode(',', $values), $token);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updateItem(RateLimitItemInterface $item, array $limits) : bool
	{
		$value = gmdate('Y-m-d\TH:i:s\Z') . ';' . implode(',', $limits);

		return $this->memcached->cas($item->getToken(), $item->getKey(), $value);
	}

	/**
	 * {@inheritDoc}
	 */
	public function insert(string $key, array $limits) : bool
	{
		$value = gmdate('Y-m-d\TH:i:s\Z') . ';' . implode(',', $limits);

		return $this->memcached->set($key, $value);
	}
}