<?php namespace mef\RateLimit\RateLimitItem;

use DateTimeImmutable;

class MemcachedRateLimitItem implements RateLimitItemInterface
{
	private $key = '';
	private $storageTime;
	private $limits = [];
	private $token = 0.0;

	/**
	 * Constructs the Memcached rate limit item
	 *
	 * @param   string              $key          Storage Key of the item
	 * @param   \DateTimeImmutable  $storageTime  Storage time
	 * @param   array               $limits       Limits
	 * @param   float               $token        Mecached cas token
	 */
	public function __construct(string $key, DateTimeImmutable $storageTime, array $limits, float $token)
	{
		$this->key = $key;
		$this->storageTime = $storageTime;
		$this->limits = $limits;
		$this->token = $token;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getKey() : string
	{
		return $this->key;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getToken() : float
	{
		return $this->token;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getStorageTime() : DateTimeImmutable
	{
		return $this->storageTime;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getRemainingLimits() : array
	{
		return $this->limits;
	}

	/**
	 * {@inheritDoc}
	 */
	public function exists() : bool
	{
		return $this->token > 0;
	}
}