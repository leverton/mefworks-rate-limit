<?php namespace mef\RateLimit\RateLimitItem;

use DateTimeImmutable;

/**
 * Information on a particular key.
 */
interface RateLimitItemInterface
{
	/**
	 * Return the key for the rate limit.
	 *
	 * @return string key
	 */
	public function getKey() : string;

	/**
	 * Returns the date and time the item was stored.
	 *
	 * @return DateTimeImmutable
	 */
	public function getStorageTime() : DateTimeImmutable;

	/**
	 * Returns the limits.
	 *
	 * @return array
	 */
	public function getRemainingLimits() : array;

	/**
	 * Returns the exists status
	 *
	 * @return bool
	 */
	public function exists() : bool;
}