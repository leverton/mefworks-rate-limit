<?php namespace mef\RateLimit\RateLimitItem;

use DateTimeImmutable;
use UnexpectedValueException;

class NullRateLimitItem implements RateLimitItemInterface
{
	/**
	 * {@inheritDoc}
	 * @throws UnexpectedValueException
	 */
	public function getKey() : string
	{
		throw new UnexpectedValueException('Invalid key for null item');
	}

	/**
	 * {@inheritDoc}
	 * @throws UnexpectedValueException
	 */
	public function getToken() : float
	{
		throw new UnexpectedValueException('Invalid token for null item');
	}

	/**
	 * {@inheritDoc}
	 * @throws UnexpectedValueException
	 */
	public function getStorageTime() : DateTimeImmutable
	{
		throw new UnexpectedValueException('Invalid storage time for null item');
	}

	/**
	 * {@inheritDoc}
	 * @throws UnexpectedValueException
	 */
	public function getRemainingLimits() : array
	{
		throw new UnexpectedValueException('Invalid remaining limits for null item');
	}

	/**
	 * {@inheritDoc}
	 */
	public function exists() : bool
	{
		return false;
	}
}