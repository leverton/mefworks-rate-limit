<?php namespace mef\RateLimit;

use DateInterval;
use DateTimeImmutable;
use mef\RateLimit\Exception\RateLimitExceededException;
use mef\RateLimit\Exception\RateLimitTimeoutException;
use mef\RateLimit\DataStore\DataStoreInterface;


/**
 * The RateLimiter is used to limit a certain action from occuring more
 * frequently than desired.
 *
 * For instance, it could be used to limit the number of requests an
 * IP address makes to an API or to limit the number of failed password
 * accounts for a particular user.
 *
 * The keys that are used should not generally be shared among different
 * users because the underlying data storage may need to do some exclusive
 * locking.
 */
class RateLimiter
{
	const CACHE_RETRY_COUNT = 5;

	/**
	 * The data storage used to save the limit data.
	 *
	 * @var DataStoreInterface
	 */
	private $cache;

	/**
	 * Max retry count
	 *
	 * @var int
	 */
	private $maxRetryCount;

	/**
	 * Constructor
	 *
	 * @param DataStoreInterface   $cache           The data storage implementation.
	 * @param int                  $maxRetryCount   Cache retry count
	 */
	public function __construct(DataStoreInterface $cache, int $maxRetryCount = self::CACHE_RETRY_COUNT)
	{
		$this->cache = $cache;
		$this->maxRetryCount = $maxRetryCount;
	}

	/**
	 * Evaluates the given key and the configured limits
	 *
	 * @param string  $key    Item key
	 * @param string  $array  Rate limit configuration for the given key
	 *
	 * @throws \mef\Exception\RateLimitTimeoutException Error for timeout
	 *
	 * @return array Remaining limits
	 */
	public function rateLimit(string $key, array $limits) : array
	{
		$tries = 0;
		$i = 0;

		do
		{
			if (++$tries === $this->maxRetryCount)
			{
				throw new RateLimitTimeoutException;
			}

			$cachedItem = $this->cache->getItem($key);

			if ($cachedItem->exists())
			{
				$now = new DateTimeImmutable('now UTC');
				$lastDate = $cachedItem->getStorageTime();
				$elapsedSeconds = $now->getTimeStamp() - $lastDate->getTimeStamp();

				/*
				 * Calculate how much time has elapsed since the prior storage
				 * time and regenerate a proportional amount of tries.
				 */
				$remainingLimits = [];

				foreach ($limits as $i => $limit)
				{
					$interval = new DateInterval($limit['period']);
					$endOfPeriod = $lastDate->add($interval);
					$periodInSeconds = $endOfPeriod->getTimeStamp() - $lastDate->getTimeStamp();
					$missedPeriods = $elapsedSeconds / $periodInSeconds;

					// Cap the limit to the maximum amount allowed in the period.
					$remainingLimits[] = min(
						$cachedItem->getRemainingLimits()[$i] + (int)($limit['limit'] * $missedPeriods),
						$limit['limit']
					);
				}

				$remainingLimits = self::decrementLimits($remainingLimits, $limits);
				$success = $this->cache->updateItem($cachedItem, $remainingLimits);
			}
			else
			{
				$remainingLimits = self::decrementLimits(array_map(function($limit){ return $limit['limit'];}, $limits), $limits);
				$success = $this->cache->insert($key, $remainingLimits);
			}
		}
		while (false === $success);

		return $remainingLimits;
	}

	/**
	 * Decrement the remaining limits
	 *
	 * @param string  $remainingLimits  Remaining limits
	 * @param string  $limits           Full limit setup of the key
	 *
	 * @throws \mef\Exception\RateLimitExceededException Error for limit exceeded
	 *
	 * @return array Remaining limits
	 */
	static private function decrementLimits(array $remainingLimits, array $limits)
	{
		for ($i = 0; $i < count($remainingLimits); ++$i)
		{
			$remainingLimits[$i]--;

			if ($remainingLimits[$i] < 0)
			{
				throw new RateLimitExceededException($limits[$i]['limit'], $limits[$i]['period']);
			}
		}

		return $remainingLimits;
	}
}