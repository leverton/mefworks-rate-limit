<?php

$app = include __DIR__ . '/../../bin/cli-app.php';
$mc = $app->getServicesContainer()['memcached'];

/**
 * An exception thrown when the rate limit has been exceeded.
 */
class RateLimitExceededException extends Exception
{
	/**
	 * Constructor
	 *
	 * @param int    $limit        The maximum number of requests per interval.
	 * @param string $intervalSpec The interval specification.
	 */
	public function __construct(int $limit, string $intervalSpec)
	{
		parent::__construct('Rate limit of ' . $limit . ' over ' . $intervalSpec . ' exceeded.');
	}
}

/**
 * An exception thrown if the rate limiter did not respond in time.
 *
 * This typically indicates that there is an extremely high load, and
 * should be treated as an error.
 */
class RateLimitTimeoutException extends Exception
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct('A timeout occurred when checking the rate limit.');
	}
}

/**
 * Information on a particular key.
 */
interface RateLimitItemInterface
{
	/**
	 * Return the key for the rate limit.
	 *
	 * @return string key
	 */
	public function getKey() : string;

	/**
	 * Returns the date and time the item was stored.
	 *
	 * @return DateTimeImmutable
	 */
	public function getStorageTime() : DateTimeImmutable;

	/**
	 * Returns the limits.
	 *
	 * @return array
	 */
	public function getRemainingLimits() : array;
}

/**
 * A data store for the rate limiter.
 */
interface DataStoreInterface
{
	/**
	 * Retrieve a previously stored value.
	 *
	 * @param  string $key  The item's key.
	 *
	 * @return mixed RateLimitItemInterface|null
	 */
	public function getItem(string $key);

	/**
	 * Update a previously retrieved item.
	 *
	 * @param  RateLimitItemInterface $item     The item that is to be updatd.
	 * @param  string                 $newValue The new value.
	 *
	 * @return bool  Returns true if the item was saved.
	 */
	public function updateItem(RateLimitItemInterface $item, array $values) : bool;
}

/**
 * A Memcached data store.
 */
class MemcachedDataStore implements DataStoreInterface
{
	private $memcached;

	public function __construct(Memcached $memcached)
	{
		$this->memcached = $memcached;
	}

	public function getItem(string $key)
	{
		$memcachedObject = $this->memcached->get($key, null, Memcached::GET_EXTENDED);

		if ($memcachedObject === false)
		{
			return null;
		}

		$value = $memcachedObject['value'];
		$token = $memcachedObject['cas'];

		list($storageTime, $values) = explode(";", $value);

		return new MemcachedRateLimitItem($key, new DateTimeImmutable($storageTime), explode(',', $values), $token);
	}

	public function updateItem(RateLimitItemInterface $item, array $limits) : bool
	{
		$value = gmdate('Y-m-d\TH:i:s\Z') . ';' . implode(',', $limits);

		return $this->memcached->cas($item->getToken(), $item->getKey(), $value);
	}

	public function insert(string $key, array $limits) : bool
	{
		$value = gmdate('Y-m-d\TH:i:s\Z') . ';' . implode(',', $limits);

		error_log('set, key: ' . $key . ', value: ' . $value);

		return $this->memcached->set($key, $value);
	}
}

class MemcachedRateLimitItem implements RateLimitItemInterface
{
	private $key = '';
	private $storageTime;
	private $limits = [];
	private $token = 0.0;

	public function __construct(string $key, DateTimeImmutable $storageTime, array $limits, float $token)
	{
		$this->key = $key;
		$this->storageTime = $storageTime;
		$this->limits = $limits;
		$this->token = $token;
	}

	public function getKey() : string
	{
		return $this->key;
	}

	public function getToken() : float
	{
		return $this->token;
	}

	public function getStorageTime() : DateTimeImmutable
	{
		return $this->storageTime;
	}

	public function getRemainingLimits() : array
	{
		return $this->limits;
	}
}

/**
 * The RateLimiter is used to limit a certain action from occuring more
 * frequently than desired.
 *
 * For instance, it could be used to limit the number of requests an
 * IP address makes to an API or to limit the number of failed password
 * accounts for a particular user.
 *
 * The keys that are used should not generally be shared among different
 * users because the underlying data storage may need to do some exclusive
 * locking.
 */
class RateLimiter
{
	const CACHE_RETRY_COUNT = 5;

	/**
	 * The data storage used to save the limit data.
	 *
	 * @var DataStoreInterface
	 */
	private $cache;

	/**
	 * Constructor
	 *
	 * @param DataStoreInterface $cache The data storage implementation.
	 */
	public function __construct(DataStoreInterface $cache)
	{
		$this->cache = $cache;
	}

	public function rateLimit(string $key, array $limits) : array
	{
		$tries = 0;
		$i = 0;

		do
		{
			if (++$tries === self::CACHE_RETRY_COUNT)
			{
				throw new RateLimitTimeoutException;
			}

			$cachedItem = $this->cache->getItem($key);

			if ($cachedItem)
			{
				$now = new DateTimeImmutable('now UTC');
				$lastDate = $cachedItem->getStorageTime();
				$elapsedSeconds = $now->getTimeStamp() - $lastDate->getTimeStamp();

				/*
				 * Calculate how much time has elapsed since the prior storage
				 * time and regenerate a proportional amount of tries.
				 */
				$remainingLimits = [];

				foreach ($limits as $i => $limit)
				{
					$interval = new DateInterval($limit['period']);
					$endOfPeriod = $lastDate->add($interval);
					$periodInSeconds = $endOfPeriod->getTimeStamp() - $lastDate->getTimeStamp();
					$missedPeriods = $elapsedSeconds / $periodInSeconds;

					// Cap the limit to the maximum amount allowed in the period.
					$remainingLimits[] = min(
						$cachedItem->getRemainingLimits()[$i] + (int)($limit['limit'] * $missedPeriods),
						$limit['limit']
					);
				}

				$remainingLimits = self::decrementLimits($remainingLimits, $limits);
				$success = $this->cache->updateItem($cachedItem, $remainingLimits);
			}
			else
			{
				$remainingLimits = self::decrementLimits(array_map(function($limit){ return $limit['limit'];}, $limits), $limits);
				$success = $this->cache->insert($key, $remainingLimits);
			}
		}
		while (false === $success);

		return $remainingLimits;
	}

	static private function decrementLimits(array $remainingLimits, $limits)
	{
		for ($i = 0; $i < count($remainingLimits); ++$i)
		{
			$remainingLimits[$i]--;

			if ($remainingLimits[$i] < 0)
			{
				throw new RateLimitExceededException($limits[$i]['limit'], $limits[$i]['period']);
			}
		}

		return $remainingLimits;
	}
}

$limiter = new RateLimiter(new MemcachedDataStore($mc));

while (true)
{
	$remainingLimits = $limiter->rateLimit('xyz', [
		['period' => 'PT1S', 'limit' => 2],
		['period' => 'PT1M', 'limit' => 120]
	]);
	usleep(400000); // 4/10th of a second. It will break on the PT1S after a few tries.
	echo print_r($remainingLimits, true);
}