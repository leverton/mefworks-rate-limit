<?php

require_once __DIR__ . '/../vendor/autoload.php';

use mef\RateLimit\RateLimiter;
use mef\RateLimit\DataStore\MemcachedDataStore;

$mc = new \Memcached();
$mc->addServer('127.0.0.1', 11211);

$limiter = new RateLimiter(new MemcachedDataStore($mc));

while (true)
{
	$remainingLimits = $limiter->rateLimit('xyz', [
		['period' => 'PT1S', 'limit' => 2],
		['period' => 'PT1M', 'limit' => 120]
	]);
	usleep(400000); // 4/10th of a second. It will break on the PT1S after a few tries.
	echo print_r($remainingLimits, true);
}